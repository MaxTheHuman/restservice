import unittest

TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWF4IiwiZXhwIjoxNjU2NTMxMDQ1LCJ0b2tlbl9wYWlyX2lkIjoxfQ.JpHuWqSUdVj_5DLBTu4wj-wlybI9lcIxBe6knm4SoPA'

def Register(self):
    return self.app.post('/register', headers={'username': 'Max', 'password': 'password'})

def Auth(self):
    return self.app.post('/auth', headers={'username': 'Max', 'password': 'password'})

def test_register(self):
    response = Register(self)
    expected_result = {"message": "User registered"}
    assert response.data == expected_result

def test_auth(self):
    Register(self)
    response = Auth(self)
    
    assert 'token' in response.data
    assert 'refresh_token' in response.data

def test_add_items(self):
    Register(self)
    Auth(self)

    response = self.app.post(
        '/catalog',
        headers={'username': 'Max', 'token': TOKEN},
        body={
            "name":"Asus",
            "category":"laptop"
        }
    )
    expected_response = {
        "item_added": {
            "id": 2,
            "info": {
                "name": "Asus",
                "category": "laptop"
            }
        }
    }

    assert response.data == expected_response

def test_get_items(self):
    Register(self)
    Auth(self)

    response = self.app.get('/catalog', headers={'username': 'Max', 'token': TOKEN})

def test_edit_items(self):
    Register(self)
    Auth(self)

    response = self.app.put(
        '/catalog',
        headers={'username': 'Max', 'token': TOKEN},
        body={
            "id": 2
            "name":"HP",
            "category":"laptop"
        }
    )

    expected_response = {
        "item_edited": {
            "id": 2,
            "info": {
                "name": "HP",
                "category": "laptop"
            }
        }
    }

    assert response.data == expected_response

def test_delete_items(self):
    Register(self)
    Auth(self)

    response = self.app.delete(
        '/catalog',
        headers={'username': 'Max', 'token': TOKEN},
        body={"id": 2}
    )

    expected_response = {
        "item_deleted": {
            "id": 2,
            "info": {
                "name": "HP",
                "category": "laptop"
            }
        }
    }

    assert response.data == expected_response

